# syslog-rabbitmq

it takes the specs from the client and gives them to the engineers

## Usage

### Client 

Check the examples/syslog-ng.conf for how to configure a client to send logs to the server. 

### Build and Run

```docker-compose up --build -d```

### Customizing 

Currently it spits out syslogs to rabbitmq in json, you can customize what fields and information is present in those logs
by modifying the syslog-ng/syslog-ng.conf file for the syslog-ng container. 

Check the "body" of the amqp destination. This is just how I did it for testing. 

You can declare additional exchanges, queues, etc. in the rabbitmq/definitions.json file.

### Security 

While the mgmt interface for rabbitmq listens on localhost, it is still configured with guest:guest as the user:pass
 obviously this is not secure. You can change it. Or set it in rabbitmq/definitions.json.  

### Consuming

Included in examples in an example of a consumer in python with pika. Based on the rabbitmq tutorials. (mad props to those folks!) 

The consumer is unbuffered so logs will come in as they do. But it still may take a second once syslog starts.

### etc. 

This command will emit a log into syslog for fun.

```logger -p kern.info "the kernel is like.. happening, dude."```
