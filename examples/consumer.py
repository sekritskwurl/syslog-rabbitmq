#!/usr/bin/env python3 -u
import pika
import json
from pprint import pprint



connection = pika.BlockingConnection(
    pika.ConnectionParameters(host='127.0.0.1'))
channel = connection.channel()

channel.exchange_declare(exchange='syslog-ng',exchange_type='direct',  durable=True )

result = channel.queue_declare(queue='',  exclusive=True)
queue_name = result.method.queue

# everything

levels = ["emergency",
          "alert", 
          "critical", 
          "error", 
          "warning",
          "notice",
          "info", 
          "debug"]

for level in levels:
    channel.queue_bind(exchange='syslog-ng', queue=queue_name, routing_key=level)

print(' [*] Waiting for logs. To exit press CTRL+C')

def callback(ch, method, properties, body):

    pprint(json.loads(body))

channel.basic_consume(
    queue=queue_name, on_message_callback=callback, auto_ack=True)

channel.start_consuming()
